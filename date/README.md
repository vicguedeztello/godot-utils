### Naive implementation of date-time manipulation class. Does not support negative unix timestamps.

### Class enums

- `Interval`
	- `SECOND`
	- `MINUTE`
	- `HOUR`
	- `DAY`
	- `WEEK`
	- `MONTH`
	- `YEAR`

### Class properties

- `year : int`
- `month : int`
- `day : int`
- `yearday : int`
- `weekday : int`
- `week : int`
- `hours : int`
- `minutes : int`
- `seconds : int`
- `leap_year : bool`

### Class methods:

- `reset()` **returns** `Date` **self**

	- Resets to default values.
- `get_unix()` **returns** `int`

	- Returns the unix timestamp.

- `current()` **returns** `Date` **self**

	- Sets the instance to current `UTC+00` date and time.

- `from_unix(unix : int)` **returns** `Date` **self**

	- Sets the instance date-time from arbitrary unix timestamp.

- `from_string(t_date : String, format : String = DEFAULT_FORMAT)` **returns** `Date` **self**

	- Sets the instance date-time from a string in specific format.

- `offset(interval : int, amount : int)` **returns** `Date` **self**

	- Add or substract an specific `Interval` to or from the instance.

- `set_time(t_hours : int = INVALID, t_minutes : int = INVALID, t_seconds : int = INVALID)` **returns** `Date` **self**

	- Sets the instance time to desired. Pass `Date.INVALID` to ignore an interval.

- `set_begin_of_month()` **returns** `Date` **self**

	- Sets the instance to the first day of the current month. **Does not** modify time.

- `set_end_of_month()` **returns** `Date` **self**

	- Sets the instance to the last day of the current month. **Does not** modify time.

- `set_next_month()` **returns** `Date` **self**

	- Sets the instance to the first day of the next month. **Does not** modify time.

- `set_previous_month()` **returns** `Date` **self**

	- Sets the instance to the first day of the previous month. **Does not** modify time.

- `format(t_format : String = DEFAULT_FORMAT)` **returns** `String`

	- Returns date and time from the instance in the format specified.

### Static methods

- `is_leap_year(t_year : int)` **returns** `bool`

	- Returns true if the year is leap.
- `days_per_month(t_year : int)` **returns** `Array`

	- Returns an array with the number of days per month. **NOTE: Index 0 will return 0**
- `days_of_month(t_year : int, t_month : int)` **returns** `int`

	- Returns the number of days in that month.
- `days_in_year(t_year : int)` **returns** `int`

	- Returns the number of days in that year.

### Formatting

Default format is `YYYY-MM-DD hh:mm:ss`. This being a simple implementation only supports the following placeholders:

- `YYYY` for year (1970).
- `MM` for month (01...12).
- `DD` for day of the month (01...31).
- `hh` for hours (00...23).
- `mm` for minutes (00...59).
- `ss` for seconds (00...59).
- `T` time alias for `hh:mm:ss`

### Code examples

```
# Returns date and time in UTC+00
var date : Date = Date.new().from_string('2020-11-25 10:30:30')

print(date.format()) # 2020-11-25 10:30:30

# Returns date and time in UTC-05
var offset_date : Date = Date.new().from_unix(date.get_unix()).offset(Date.Interval.HOUR, -5)

print(offset_date.format('DD-MM-YYYY T')) # 25-11-2020 05:30:30
```
