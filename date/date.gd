class_name Date
extends Reference
# Docs


# Signals



# Enums
enum Interval {
	SECOND,
	MINUTE,
	HOUR,
	DAY,
	WEEK,
	MONTH,
	YEAR,
	}


# Constants
const INVALID : int = -1

const DEFAULT_UNIX_TIME : int = 0
const DEFAULT_YEAR : int = 1970
const DEFAULT_MONTH : int = 1
const DEFAULT_DAY : int = 1
const DEFAULT_YEARDAY : int = 1
const DEFAULT_WEEKDAY : int = 4
const DEFAULT_WEEK : int = 1
const DEFAULT_HOURS : int = 0
const DEFAULT_MINUTES : int = 0
const DEFAULT_SECONDS : int = 0
const DEFAULT_LEAP_YEAR : bool = false
const DEFAULT_FORMAT : String = 'YYYY-MM-DD T'

const SECONDS_IN_MINUTE : int = 60
const SECONDS_IN_HOUR : int = 3600
const SECONDS_IN_DAY : int = 86400
const SECONDS_IN_WEEK : int = 604800
const SECONDS_IN_MONTH : int = 2678400
const SECONDS_IN_YEAR : int = 31556926

const DAYS_IN_WEEK : int = 7
const DAYS_IN_YEAR : int = 365
const DAYS_IN_LEAP_YEAR : int = 366

const MONTHS_IN_YEAR : int = 12


# Exported vars



# Public vars
var year : int = DEFAULT_YEAR setget _set_year
var month : int = DEFAULT_MONTH setget _set_month
var day : int = DEFAULT_DAY setget _set_day
var yearday : int = DEFAULT_YEARDAY setget _set_yearday
var weekday : int = DEFAULT_WEEKDAY setget _set_weekday
var week : int = DEFAULT_WEEK setget _set_week
var hours : int = DEFAULT_HOURS setget _set_hours
var minutes : int = DEFAULT_MINUTES setget _set_minutes
var seconds : int = DEFAULT_SECONDS setget _set_seconds
var leap_year : bool = DEFAULT_LEAP_YEAR setget _set_leap_year


# Private vars
var _unix : int = DEFAULT_UNIX_TIME setget _set_unix


# Onready vars



# Built-in funcs
#func _init():
#	pass


# Public funcs
func reset() -> Reference:
	_unix = DEFAULT_UNIX_TIME
	
	year = DEFAULT_YEAR
	month = DEFAULT_MONTH
	day = DEFAULT_DAY
	yearday = DEFAULT_YEARDAY
	weekday = DEFAULT_WEEKDAY
	week = DEFAULT_WEEK
	hours = DEFAULT_HOURS
	minutes = DEFAULT_MINUTES
	seconds = DEFAULT_SECONDS
	leap_year = DEFAULT_LEAP_YEAR
	
	return self


func get_unix() -> int:
	return _unix


func current(utc_time : bool = false) -> Reference:
	_unix = OS.get_unix_time()
	
	_update()
	
	return self if utc_time == true else offset(Interval.MINUTE, OS.get_time_zone_info().bias)


func from_unix(unix : int) -> Reference:
	_unix = unix
	
	_update()
	
	return self


func from_string(date : String, format : String = DEFAULT_FORMAT) -> Reference:
	format = format.replace('T', 'hh:mm:ss')
	
	var calculated_unix : int = DEFAULT_UNIX_TIME
	var t_year : int = INVALID
	
	while date.length() > 0:
		if format.begins_with('YYYY') == true:
			t_year = int(date.left(4))
			
			var i_year : int = DEFAULT_YEAR
			
			while i_year < t_year:
				calculated_unix += (days_in_year(i_year) * SECONDS_IN_DAY)
				
				i_year += 1
			
			format.erase(0, 4)
			date.erase(0, 4)
		elif format.begins_with('MM') == true:
			if t_year == INVALID:
				# warning-ignore:return_value_discarded
				format += ' MM'
				# warning-ignore:return_value_discarded
				date += ' %s' % date.left(2)
			else:
				var t_month : int = int(date.left(2))
				var i_month : int = DEFAULT_MONTH
				
				while i_month < t_month:
					calculated_unix += (days_of_month(t_year, i_month) * SECONDS_IN_DAY)
					
					i_month += 1
			
			format.erase(0, 2)
			date.erase(0, 2)
		elif format.begins_with('DD') == true:
			var t_days : int = int(date.left(2)) - DEFAULT_DAY
			
			calculated_unix += (t_days * SECONDS_IN_DAY)
			
			format.erase(0, 2)
			date.erase(0, 2)
		elif format.begins_with('hh') == true:
			calculated_unix += (int(date.left(2)) * SECONDS_IN_HOUR)
			
			format.erase(0, 2)
			date.erase(0, 2)
		elif format.begins_with('mm') == true:
			calculated_unix += (int(date.left(2)) * SECONDS_IN_MINUTE)
			
			format.erase(0, 2)
			date.erase(0, 2)
		elif format.begins_with('ss') == true:
			calculated_unix += int(date.left(2))
			
			format.erase(0, 2)
			date.erase(0, 2)
		else:
			format.erase(0, 1)
			date.erase(0, 1)
	
	_unix = calculated_unix
	
	_update()
	
	return self


func offset(interval : int, amount : int) -> Reference:
	match interval:
		Interval.SECOND:
			_unix += amount
		Interval.MINUTE:
			_unix += (amount * SECONDS_IN_MINUTE)
		Interval.HOUR:
			_unix += (amount * SECONDS_IN_HOUR)
		Interval.DAY:
			_unix += (amount * SECONDS_IN_DAY)
		Interval.WEEK:
			_unix += (amount * SECONDS_IN_WEEK)
		Interval.MONTH:
			_unix += (amount * SECONDS_IN_MONTH)
		Interval.YEAR:
			_unix += (amount * SECONDS_IN_YEAR)
	
	_update()
	
	return self


func set_time(t_hours : int = INVALID, t_minutes : int = INVALID, t_seconds : int = INVALID) -> Reference:
	if not t_hours == INVALID:
		# warning-ignore:return_value_discarded
		offset(Interval.HOUR, t_hours - hours)
	
	if not t_minutes == INVALID:
		# warning-ignore:return_value_discarded
		offset(Interval.MINUTE, t_minutes - minutes)
	
	if not t_seconds == INVALID:
		# warning-ignore:return_value_discarded
		offset(Interval.SECOND, t_seconds - seconds)
	
	return self


func set_begin_of_month() -> Reference:
	# warning-ignore:return_value_discarded
	offset(Interval.DAY, 1 - day)
	
	return self


func set_end_of_month() -> Reference:
	# warning-ignore:return_value_discarded
	offset(Interval.DAY, days_of_month(year, month) - day)
	
	return self


func set_next_month() -> Reference:
	# warning-ignore:return_value_discarded
	set_end_of_month()
	# warning-ignore:return_value_discarded
	offset(Interval.DAY, 1)
	
	return self


func set_previous_month() -> Reference:
	# warning-ignore:return_value_discarded
	set_begin_of_month()
	# warning-ignore:return_value_discarded
	offset(Interval.DAY, -1)
	# warning-ignore:return_value_discarded
	set_begin_of_month()
	
	return self


func format(t_format : String = DEFAULT_FORMAT) -> String:
	t_format = t_format.replace('T', 'hh:mm:ss')
	
	t_format = t_format.replace('YYYY', '%04d' % year)
	t_format = t_format.replace('MM', '%02d' % month)
	t_format = t_format.replace('DD', '%02d' % day)
	t_format = t_format.replace('hh', '%02d' % hours)
	t_format = t_format.replace('mm', '%02d' % minutes)
	t_format = t_format.replace('ss', '%02d' % seconds)
	
	return t_format


static func is_leap_year(t_year : int) -> bool:
	var mod_four : int = t_year % 4
	var mod_one_h : int = t_year % 100
	var mod_four_h : int = t_year % 400
	
	if (mod_four_h == 0) or (mod_four == 0 and not mod_one_h == 0):
		return true
	else:
		return false


static func days_per_month(t_year : int) -> Array:
	return [0, 31, 29 if is_leap_year(t_year) else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


static func days_of_month(t_year : int, t_month : int) -> int:
	var days : Array = days_per_month(t_year)
	
	return days[t_month]


static func days_in_year(t_year : int) -> int:
	return DAYS_IN_LEAP_YEAR if is_leap_year(t_year) else DAYS_IN_YEAR


# Private funcs
func _update() -> void:
	# warning-ignore:integer_division
	var days_in_unix : int = _unix / SECONDS_IN_DAY
	
	weekday = _calculate_weekday(days_in_unix)
	leap_year = is_leap_year(year)
	
	year = DEFAULT_YEAR
	
	while days_in_unix >= days_in_year(year):
		days_in_unix -= days_in_year(year)
		year += 1
	
	yearday = DEFAULT_YEARDAY + days_in_unix
	# warning-ignore:integer_division
	week = DEFAULT_WEEK + (days_in_unix / DAYS_IN_WEEK)
	
	var days_per_month : Array = days_per_month(year)
	
	month = DEFAULT_MONTH
	
	while days_in_unix >= days_per_month[month]:
		days_in_unix -= days_per_month[month]
		month += 1
	
	day = DEFAULT_DAY + days_in_unix
	
	# Amount of seconds elapsed on this day
	var seconds_in_day : int = _unix % SECONDS_IN_DAY
	
	# warning-ignore:integer_division
	hours = seconds_in_day / SECONDS_IN_HOUR
	# warning-ignore:integer_division
	minutes = (seconds_in_day % SECONDS_IN_HOUR) / SECONDS_IN_MINUTE
	seconds = seconds_in_day % SECONDS_IN_MINUTE


func _calculate_weekday(t_days : int) -> int:
	var t_weekday : int = (DEFAULT_WEEKDAY + t_days) % DAYS_IN_WEEK
	
	return t_weekday


func _set_unix(_new_value : int) -> void:
	push_error('Invalid unix time assignment')


func _set_year(_new_value : int) -> void:
	push_error('Invalid year assignment')


func _set_month(_new_value : int) -> void:
	push_error('Invalid month assignment')


func _set_day(_new_value : int) -> void:
	push_error('Invalid day assignment')


func _set_yearday(_new_value : int) -> void:
	push_error('Invalid yearday assignment')


func _set_weekday(_new_value : int) -> void:
	push_error('Invalid weekday assignment')


func _set_week(_new_value : int) -> void:
	push_error('Invalid week assignment')


func _set_hours(_new_value : int) -> void:
	push_error('Invalid hours assignment')


func _set_minutes(_new_value : int) -> void:
	push_error('Invalid minutes assignment')


func _set_seconds(_new_value : int) -> void:
	push_error('Invalid seconds assignment')


func _set_leap_year(_new_value : bool) -> void:
	push_error('Invalid leap year assignment')

