extends Node
# Docs


# Signals



# Enums



# Constants



# Exported vars



# Public vars



# Private vars



# Onready vars



# Built-in funcs
#func _init():
#	pass


func _ready():
	# Returns date and time in UTC+00
	var date : Date = Date.new().from_string('2020-11-25 10:30:30')
	
	print(date.format()) # 2020-11-25 10:30:30
	
	# Returns date and time in UTC-05
	var offset_date : Date = Date.new().from_unix(date.get_unix()).offset(Date.Interval.HOUR, -5)
	
	print(offset_date.format('DD-MM-YYYY T')) # 25-11-2020 05:30:30


#func _exit_tree():
#	pass


# Public funcs



# Private funcs



