class_name QuatCamera
extends Camera
# Docs


# Signals



# Enums



# Constants
const POSITIVE_SIGN : int = 1
const NEGATIVE_SIGN : int = -1

const DEGREE_DEFAULT : float = 0.0
const DEGREE_HALF : float = 180.0
const DEGREE_MAX : float = 360.0

const ZOOM_DEFAULT : float = 0.0

const ANGLE_UP : Vector3 = Vector3.RIGHT
const ANGLE_DOWN : Vector3 = Vector3.LEFT
const ANGLE_RIGHT : Vector3 = Vector3.DOWN
const ANGLE_LEFT : Vector3 = Vector3.UP
const ANGLE_TILT_LEFT : Vector3 = Vector3.BACK
const ANGLE_TILT_RIGHT : Vector3 = Vector3.FORWARD


# Exported vars
export(float) var yaw : float = DEGREE_DEFAULT setget _set_yaw
export(float) var pitch : float = DEGREE_DEFAULT setget _set_pitch
export(float) var roll : float = DEGREE_DEFAULT setget _set_roll
export(float) var zoom : float = ZOOM_DEFAULT setget _set_zoom
export(float) var zoom_out_max : float = ZOOM_DEFAULT setget _set_zoom_out_max
export(float) var zoom_in_max : float = ZOOM_DEFAULT setget _set_zoom_in_max
export(bool) var invert_yaw : bool = false setget _set_invert_yaw
export(bool) var invert_pitch : bool = false setget _set_invert_pitch
export(bool) var invert_roll : bool = false setget _set_invert_roll


# Public vars



# Private vars



# Onready vars
onready var base_translation : Vector3 = self.translation


# Built-in funcs
#func _init() -> void:
#	pass


#func _ready() -> void:
#	pass


#func _exit_tree() -> void:
#	pass


# Public funcs
static func get_shortest(start : float, end : float) -> float:
	start = get_absolute(start)
	end = get_absolute(end)
	
	var remainder : float = fposmod(end - start + DEGREE_HALF, DEGREE_MAX)
	var difference : float = remainder - DEGREE_HALF
	
	if difference < -DEGREE_HALF:
		return difference + DEGREE_MAX
	
	return difference


static func get_absolute(degree : float) -> float:
	return wrapf(degree, DEGREE_DEFAULT, DEGREE_MAX)


func update_base_translation() -> void:
	base_translation = self.translation


# Private funcs
func _set_invert_yaw(value : bool) -> void:
	invert_yaw = value
	
	_update_rotation()


func _set_invert_pitch(value : bool) -> void:
	invert_pitch = value
	
	_update_rotation()


func _set_invert_roll(value : bool) -> void:
	invert_roll = value
	
	_update_rotation()


func _set_yaw(value : float) -> void:
	yaw = fmod(value, DEGREE_MAX)
	
	_update_rotation()


func _set_pitch(value : float) -> void:
	pitch = fmod(value, DEGREE_MAX)
	
	_update_rotation()


func _set_roll(value : float) -> void:
	roll = fmod(value, DEGREE_MAX)
	
	_update_rotation()


func _set_zoom(value : float) -> void:
	zoom = clamp(value, zoom_out_max, zoom_in_max)
	
	_update_zoom()


func _set_zoom_out_max(value : float) -> void:
	if sign(value) == POSITIVE_SIGN:
		value = value * NEGATIVE_SIGN
	
	zoom_out_max = value
	
	_set_zoom(zoom)


func _set_zoom_in_max(value : float) -> void:
	if sign(value) == NEGATIVE_SIGN:
		value = value * NEGATIVE_SIGN
	
	zoom_in_max = value
	
	_set_zoom(zoom)


func _update_zoom() -> void:
	if base_translation == null:
		return
	
	var offset : Vector3 = Vector3(0, 0, zoom * NEGATIVE_SIGN)
	
	self.translation = base_translation + self.global_transform.basis.xform(offset)


func _update_rotation() -> void:
	if not is_inside_tree():
		return
	
	var yaw_angle : Vector3 = ANGLE_LEFT if invert_yaw else ANGLE_RIGHT
	var yaw_radians : float = deg2rad(get_absolute(yaw))
	var yaw_quat : Quat = Quat(yaw_angle, yaw_radians)
	
	var pitch_angle : Vector3 = ANGLE_DOWN if invert_pitch else ANGLE_UP
	var pitch_radians : float = deg2rad(get_absolute(pitch))
	var pitch_quat : Quat = Quat(pitch_angle, pitch_radians)
	
	var roll_angle : Vector3 = ANGLE_TILT_LEFT if invert_roll else ANGLE_TILT_RIGHT
	var roll_radians : float = deg2rad(get_absolute(roll))
	var roll_quat : Quat = Quat(roll_angle, roll_radians)
	
	var new_quat : Quat = yaw_quat * pitch_quat * roll_quat
	
	self.global_transform.basis = Basis(new_quat.normalized())

