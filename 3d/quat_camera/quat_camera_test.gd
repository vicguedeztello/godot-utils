extends Spatial
# Docs


# Signals



# Enums



# Constants



# Exported vars



# Public vars



# Private vars



# Onready vars
onready var camera : QuatCamera = $quat_camera
onready var yaw_input : HSlider = $controls/yaw/display/input
onready var yaw_label : Label = $controls/yaw/display/label
onready var pitch_input : HSlider = $controls/pitch/display/input
onready var pitch_label : Label = $controls/pitch/display/label
onready var roll_input : HSlider = $controls/roll/display/input
onready var roll_label : Label = $controls/roll/display/label
onready var zoom_input : HSlider = $controls/zoom/display/input
onready var zoom_label : Label = $controls/zoom/display/label
onready var invert_yaw_checkbox : CheckBox = $controls/invert_yaw
onready var invert_pitch_checkbox : CheckBox = $controls/invert_pitch
onready var invert_roll_checkbox : CheckBox = $controls/invert_roll


# Built-in funcs
#func _init() -> void:
#	pass


func _ready() -> void:
	# Rotate 10 degrees to the right.
	camera.yaw = 10
	# Rotate 10 degrees up.
	camera.pitch = 10
	# Tilt 90 degrees to the right.
	camera.roll = 90
	
	# Rotate to -340 (absolute is 20 degrees) using shortest path, which is 10
	# degrees to the right since we are already rotated 10 degrees to the right.
	camera.yaw += camera.get_shortest(camera.yaw, -340)
	
	# warning-ignore:return_value_discarded
	yaw_input.connect('value_changed', self, '_on_yaw_input_value_changed')
	# warning-ignore:return_value_discarded
	pitch_input.connect('value_changed', self, '_on_pitch_input_value_changed')
	# warning-ignore:return_value_discarded
	roll_input.connect('value_changed', self, '_on_roll_input_value_changed')
	# warning-ignore:return_value_discarded
	zoom_input.connect('value_changed', self, '_on_zoom_input_value_changed')
	# warning-ignore:return_value_discarded
	invert_yaw_checkbox.connect('toggled', self, '_on_invert_yaw_toggled')
	# warning-ignore:return_value_discarded
	invert_pitch_checkbox.connect('toggled', self, '_on_invert_pitch_toggled')
	# warning-ignore:return_value_discarded
	invert_roll_checkbox.connect('toggled', self, '_on_invert_roll_toggled')
	
	yaw_input.value = camera.yaw
	pitch_input.value = camera.pitch
	roll_input.value = camera.roll
	zoom_input.min_value = camera.zoom_out_max
	zoom_input.max_value = camera.zoom_in_max
	zoom_input.value = camera.zoom


#func _exit_tree() -> void:
#	pass


# Public funcs



# Private funcs
func _on_invert_yaw_toggled(pressed : bool) -> void:
	camera.invert_yaw = pressed


func _on_invert_pitch_toggled(pressed : bool) -> void:
	camera.invert_pitch = pressed


func _on_invert_roll_toggled(pressed : bool) -> void:
	camera.invert_roll = pressed


func _on_yaw_input_value_changed(value : float) -> void:
	yaw_label.text = str(value)
	
	camera.yaw = value


func _on_pitch_input_value_changed(value : float) -> void:
	pitch_label.text = str(value)
	
	camera.pitch = value


func _on_roll_input_value_changed(value : float) -> void:
	roll_label.text = str(value)
	
	camera.roll = value


func _on_zoom_input_value_changed(value : float) -> void:
	zoom_label.text = str(value)
	
	camera.zoom = value




