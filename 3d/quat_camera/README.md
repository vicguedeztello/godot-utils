### Naive implementation of a quaternion based camera to be manipulated using degrees.

##### Description and warnings.

This object is meant to be inherited to apply custom properties or behaviours. It only should be rotated using its `yaw`, `pitch` or `roll` properties. Also, this object will lose any scaling when a rotation or tilt gets applied to it.

##### Wrapped degrees.

It returns a value between `DEGREE_DEFAULT` and `DEGREE_MAX`. So a value of `-10` becomes `350` degrees.

### Class constants

- `DEGREE_DEFAULT : float = 0.0`
- `DEGREE_HALF : float = 180.0`
- `DEGREE_MAX : float = 360.0`
- `ZOOM_DEFAULT : float = 0.0`
- `ANGLE_UP : Vector3 = Vector3.RIGHT`
- `ANGLE_DOWN : Vector3 = Vector3.LEFT`
- `ANGLE_RIGHT : Vector3 = Vector3.DOWN`
- `ANGLE_LEFT : Vector3 = Vector3.UP`
- `ANGLE_TILT_LEFT : Vector3 = Vector3.BACK`
- `ANGLE_TILT_RIGHT : Vector3 = Vector3.FORWARD`

### Class properties

- `yaw : float`

	- Right or Left rotation in degrees, positive value rotates to the right.
- `pitch : float`

	- Up or Down rotation in degrees, positive value rotates up.
- `roll : float`

	- Right or Left tilting in degrees, positive value tilts to the right.
- `zoom : float`

	- Positive value zooms In, negative values zooms Out.
- `zoom_out_max : float`

	- Max distance of the zoom out, always a negative value.
- `zoom_in_max : float`

	- Max distance of the zoom in, always a positive value.
- `invert_yaw : bool`

	- Inverts Yaw direction.
- `invert_pitch : bool`

	- Inverts Pitch direction.
- `invert_roll : bool`

	- Inverts Roll direction.

### Class methods:

- `get_shortest(start : float, end : float)` **returns** `float`

	- Returns the shortest rotation degrees from two angles.
- `get_absolute(degrees : float)` **returns** `float`

	- Returns `degrees` wrapped.

### Code examples

```
# Get quat camera instance
var camera : QuatCamera = $quat_camera

# Rotate 10 degrees to the right.
camera.yaw = 10
# Rotate 10 degrees up.
camera.pitch = 10
# Tilt 90 degrees to the right.
camera.roll = 90

# Rotate to -340 (absolute is 20 degrees) using shortest path, which is 10
# degrees to the right since we are already rotated 10 degrees to the right.
camera.yaw += camera.get_shortest(camera.yaw, -340)
```
